%{
#include <cstdio>
#include <cstring>
#include <iostream>
#include <vector>
#include <parse_tree.h>
#include <unordered_map>
#include <curry_tree.h>
#include <equivalence_class.h>
using namespace std;

//#define YYDEBUG 1
extern "C" int yylex();
//extern "C" int yyparse();
extern "C" FILE* yyin;

void yyerror(string s);
int id = 0;
unsigned line_count = 0;
node_p* root = new node_p;
unordered_map<string, node_l*>* lookup = new unordered_map<string, node_l*>;
ParseTree parse;
%}

%union {
	int ival;
	double dval;
	char* sval;
	vector<node_p*>* vec;
	vector<string>* names;
	node_p* node;
}

%token <ival> INT
%token <dval> DOUBLE
%token <sval> IDENTIFIER
%token <sval> TYPE
%token EQUALS
%token OP
%token RIGHTPAREN
%token LEFTPAREN
%token RIGHTBRACE
%token LEFTBRACE
%token BECOMES
%token FUNC
%token CONST
%token VAR
%token PRED
%token COMMA
%token SEMICOLON
%token IMPLIES
%token PARTITION
%token FALSE
%token TRUE

%type <vec> list
%type <vec> arguments
%type <node> identity
%type <node> rule
%type <names> variable_names
%type <vec> rules
%type <vec> knowledge
%type <node> item

%start body

%%

body: declarations PARTITION rules PARTITION knowledge
{
	root->token_ = "start";
	parse.insert(root);
};

declarations: declarations declaration SEMICOLON
{
	++line_count;
}
| declaration SEMICOLON
{
	++line_count;
};

declaration: TYPE variable_names
{
	vector<string>* v = $2;

	for( int i = 0; i < v->size(); ++i ) {
		node_l* new_node = new node_l;
		new_node->name_ = v->at(i);

		if ( lookup->find(v->at(i)) == lookup->end() ) {
			if ( !strncmp($1,"const", 6) ) {
				new_node->typing_ = CONST;
			} else if ( !strncmp($1, "var", 6) ) {
				new_node->typing_ = VAR;
			} else if ( !strncmp($1, "pred", 6) ) {
				new_node->typing_ = PRED;
			} else if ( !strncmp($1, "func", 6) ) {
				new_node->typing_ = FUNC;
			} else {
				new_node->typing_ = 0;
			}

			new_node->id_number_ = id;
			++id;

			pair<string, node_l*> temp(v->at(i), new_node);
			lookup->insert(temp);
		}
	}
	delete v;
};

variable_names: variable_names COMMA IDENTIFIER
{
	$1->push_back($3);
	$$ = $1;
}

| IDENTIFIER
{
	vector<string>* v = new vector<string>;
	v->push_back($1);
	$$ = v;
};

rules: rules rule SEMICOLON
{
	++line_count;
	if( root->right_ == NULL ) {
		root->right_ = new vector<node_p*>;
	}
	root->right_->push_back($2);
	$2->parent_ = root;
}	
| rule SEMICOLON
{
	++line_count;
	if( root->right_ == NULL ) {
		root->right_ = new vector<node_p*>;
	}
	root->right_->push_back($1);
	$1->parent_ = root;
};

rule: list BECOMES list 
{
	node_p* id_node = new node_p;
	id_node->token_ = "=>";
	id_node->right_ = $3;
	id_node->left_ = $1;

	for ( auto itr = id_node->right_->begin(); itr != id_node->right_->end(); ++itr ) {
		(*itr)->parent_ = id_node;
	}

	for ( auto itr = id_node->left_->begin(); itr != id_node->left_->end(); ++itr ) {
		(*itr)->parent_ = id_node;
	}

	$$ = id_node;
}

list: list COMMA item
{
	$1->push_back($3);
	$$ = $1;
}
| item
{
	vector<node_p*>* v = new vector<node_p*>;
	v->push_back($1);
	$$ = v;
};

item: identity EQUALS identity
{
	node_p* id_node = new node_p;
    id_node->token_ = "=";
    if ( id_node->left_ == NULL ) {
        id_node->left_ = new vector<node_p*>;
    }
    if ( id_node->right_ == NULL ) {
        id_node->right_ = new vector<node_p*>;
    }
    id_node->left_->push_back($1);
    id_node->right_->push_back($3);
    $1->parent_ = id_node;
    $3->parent_ = id_node;
    $$ = id_node;
}
| identity IMPLIES identity
{
	node_p* id_node = new node_p;
    id_node->token_ = "->";
    if ( id_node->left_ == NULL ) {
        id_node->left_ = new vector<node_p*>;
    }
    if ( id_node->right_ == NULL ) {
        id_node->right_ = new vector<node_p*>;
    }
    id_node->left_->push_back($1);
    id_node->right_->push_back($3);
    $1->parent_ = id_node;
    $3->parent_ = id_node;
    $$ = id_node;
};

identity: IDENTIFIER LEFTPAREN arguments RIGHTPAREN 
{
	if ( lookup->find($1) == lookup->end() ) {
		yyerror(((string)$1 + (string)" Identifier not declared."));
	}
	node_p* id_node = new node_p;
	id_node->lookup_info_ = lookup->at($1);
	id_node->left_ = $3;

	for ( auto itr = id_node->left_->begin(); itr != id_node->left_->end(); ++itr ) {
		(*itr)->parent_ = id_node;
	}

	$$ = id_node;
}
| LEFTBRACE arguments RIGHTBRACE
{
	node_p* id_node = new node_p;
	id_node->left_ = $2;
	try{
		id_node->lookup_info_ = lookup->at("list");
	} 
	catch(...) {

		node_l* list = new node_l;
		list->name_ = "list";
		list->typing_ = FUNC;
		list->id_number_ = id;
		++id;

		pair<string, node_l*> temp("list", list);
		lookup->insert(temp);
		
		id_node->lookup_info_ = lookup->at("list");
	}

	for ( auto itr = id_node->left_->begin(); itr != id_node->left_->end(); ++itr ) {
		(*itr)->parent_ = id_node;
	}

	$$ = id_node;
}
| IDENTIFIER
{
	if ( lookup->find($1) == lookup->end() ) {
		yyerror(((string)$1 + (string)" Identifier not declared."));
	}
	node_p* id_node = new node_p;
	id_node->lookup_info_ = lookup->at($1);
	$$ = id_node;
};

arguments: arguments COMMA identity
{
	$1->push_back($3);
	$$ = $1;
}
| identity
{
	vector<node_p*>* v = new vector<node_p*>;
	v->push_back($1);
	$$ = v;
}
| /*empty*/
{
	$$ = new vector<node_p*>;
};

knowledge: knowledge item SEMICOLON
{
	if( root->left_ == NULL ) {
        root->left_ = new vector<node_p*>;
    }
    root->left_->push_back($2);
    $2->parent_ = root;
	
}
| item SEMICOLON
{
	if( root->left_ == NULL ) {
        root->left_ = new vector<node_p*>;
    }
    root->left_->push_back($1);
    $1->parent_ = root;
};


%%
void yyerror(string s) {
	cout << "Error:"<< line_count << ": " << s << endl;
	exit(-1);
}

