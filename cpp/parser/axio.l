%{
	#include <iostream>
	#include <cstdlib>
	#include <vector>
	#include <parse_node.h>
	#include "axio.tab.h"
	using namespace std;
	#define YY_DECL extern "C" int yylex()
%}

WSPACE		[ \t\n\r]+
INTEGER		[0-9]+
DECIMAL		[0-9]+\.[0-9]*
SEMICOLON	";"
COMMA		","
BECOMES		"=>"
IMPLIES		"->"
LEFTPAREN	"("
RIGHTPAREN	")"
LEFTBRACE	"{"
RIGHTBRACE	"}"
PARTITION	"%%"
IDENTIFIER	[a-zA-Z0-9_]+
OP			[+-/*%]
EQUALS		"="
FUNC		"func"
VAR			"var"
CONST		"const"
PRED		"pred"
TRUE		"True"
FALSE		"False"
%%

{FUNC}			{ yylval.sval = strdup(yytext); return TYPE; }
{CONST}			{ yylval.sval = strdup(yytext); return TYPE; }
{VAR}			{ yylval.sval = strdup(yytext); return TYPE; }
{PRED}			{ yylval.sval = strdup(yytext); return TYPE; }
{TRUE}			{ return TRUE; }
{FALSE}			{ return FALSE; }
{LEFTPAREN}		{ return LEFTPAREN; }
{RIGHTPAREN}	{ return RIGHTPAREN; }
{LEFTBRACE}		{ return LEFTBRACE; }
{RIGHTBRACE}	{ return RIGHTBRACE; }
{WSPACE}		;
{DECIMAL}		{ yylval.dval = atof(yytext); return DOUBLE; }
{INTEGER}		{ yylval.ival = atoi(yytext); return INT; }
{IDENTIFIER}	{ yylval.sval = strdup(yytext); return IDENTIFIER; }
{COMMA}			{ return COMMA; }
{IMPLIES}		{ return IMPLIES; }
{BECOMES}		{ return BECOMES; }
{EQUALS}		{ return EQUALS; }
{PARTITION}		{ return PARTITION; }
{SEMICOLON}		{ return SEMICOLON; }
{OP}			{ yylval.sval = strdup(yytext); return OP; }
.				{ cout << "[" << yytext[0] << "]"; return yytext[0]; }

%%
