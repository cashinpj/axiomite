#include <signature_table.h>
#include <curry_node.h>
using namespace std;

int main() {
	pair<int, int> s1 = make_pair(1,1);
	pair<int, int> s2 = make_pair(2,2);
	pair<int, int> s3 = make_pair(3,3);

	string n1 = "foo";
	string n2 = "bar";
	string n3 = "baz";

	node_c* v = new node_c;
	v->name_ = n3;
	v->signature_ = s3;

	SignatureTable t;

	t.enter(s1, n1);
	t.enter(s2, n2);
	t.enter(v);

	t.print();

	t.remove(s1);
	t.remove(v);

	t.print();

}
