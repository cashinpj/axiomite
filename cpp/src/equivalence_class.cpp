#include <equivalence_class.h>

vector<node_c*> EquivalenceClass::findAllEqual(string current) {
	vector<node_c*> results;

	for(int i = 0; i < equiv_classes_.size(); ++i) {
		if(isEqual(current, equiv_classes_.at(i)->name_)) {
			results.push_back(equiv_classes_.at(i)->location_);
		}
	}

	return results;
}

void EquivalenceClass::init(CurryTree& t){ 

	curry_tree_ = t;
	
	auto table_itr = t.table_begin();
	auto table_end = t.table_end();
	node_e* temp_eq;

	equiv_classes_.resize(curry_tree_.table_size());
	
	for( ; table_itr != table_end; ++table_itr ) {
		temp_eq = new node_e;
		
		temp_eq->name_ = table_itr->first;
		temp_eq->id_number_ = table_itr->second;
		temp_eq->representative_ = temp_eq;
		temp_eq->rank_ = 1;

		temp_eq->list_of_parents_ = t.list_parents(temp_eq->name_);
		temp_eq->location_ = t.findNode(temp_eq->name_);

		equiv_classes_[temp_eq->id_number_] = temp_eq;
	}
	
	//combine base on equations known
	auto root_begin = curry_tree_.root_begin();
	auto root_end = curry_tree_.root_end();
	vector<node_c*> pending;

	for( ; root_begin != root_end; ++root_begin ) {
		if( (*root_begin)->name_ == "=" ) {
			pending.push_back((*root_begin)->left_);
			pending.push_back((*root_begin)->right_);
			combine(find((*root_begin)->left_), find((*root_begin)->right_));
		}
	}	
}

void EquivalenceClass::init(ParseTree& p, unordered_map<string, node_l*>* lookup) {
	curry_tree_.init(p, lookup);
	
	init(curry_tree_);

}

//Find - returns the name of equivalence class

node_e* EquivalenceClass::find(int eq_class) {
	return find(equiv_classes_[eq_class]);
}


node_e* EquivalenceClass::find(string name) {
	int eq_class = curry_tree_.lookup(name);
	
	return find(equiv_classes_[eq_class]);
}

node_e* EquivalenceClass::find(node_c* vertex) {
	int eq_class = curry_tree_.lookup(vertex->name_);	
	
	return find(equiv_classes_[eq_class]);
}

node_e* EquivalenceClass::find(node_e* eq) {
	if (eq->representative_ != eq) {
		eq->representative_ = find(eq->representative_);
		
	}

	return eq->representative_;
}

//combine - combines two equivalence classes.

void EquivalenceClass::combine(node_e* class_one, node_e* class_two) {
	node_e* rep_one = find(class_one);
	node_e* rep_two = find(class_two);

	if ( rep_one == rep_two ) {
		return;
	} else {
		if ( rep_one->rank_ > rep_two->rank_ ) {
			rep_two->representative_ = rep_one;
			rep_one->rank_ += rep_two->rank_;
	
			//combine lists of parents into new list, store in new rep
			auto list_end = rep_one->list_of_parents_.end();
			auto list_two_begin = rep_two->list_of_parents_.begin();
			auto list_two_end = rep_two->list_of_parents_.end();

			rep_one->list_of_parents_.insert(list_end, list_two_begin, list_two_end);

		} else {
			rep_one->representative_ = rep_two;
			rep_two->rank_ += rep_one->rank_;
			
			//combine lists of parents into new list, store in new rep
			auto list_end = rep_two->list_of_parents_.end();
			auto list_one_begin = rep_one->list_of_parents_.begin();
			auto list_one_end = rep_one->list_of_parents_.end();

			rep_two->list_of_parents_.insert(list_end, list_one_begin, list_one_end);
		}
	}
}

void EquivalenceClass::combine(int class_one, int class_two) {
	combine(equiv_classes_[class_one], equiv_classes_[class_two]);
}

void EquivalenceClass::combine(string name_one, string name_two) {
	int class_one = curry_tree_.lookup(name_one);
	int class_two = curry_tree_.lookup(name_two);

	combine(equiv_classes_[class_one], equiv_classes_[class_two]);
}

void EquivalenceClass::combine(node_c* vertex_one, node_c* vertex_two) {
	int class_one = curry_tree_.lookup(vertex_one->name_);
	int class_two = curry_tree_.lookup(vertex_two->name_);
		
	
	combine(equiv_classes_[class_one], equiv_classes_[class_two]);

}

void EquivalenceClass::print() {
	curry_tree_.print();

	cout << endl << "Printing equivalences" << endl << endl;
	for ( int i = 0; i < equiv_classes_.size(); ++i ) {
		cout << equiv_classes_[i]->name_ << ": Rep " << find(i)->name_ << endl;
	}
}



//union-find

void EquivalenceClass::unionFind() {
	auto clause_itr = curry_tree_.root_begin();
	auto clause_end = curry_tree_.root_end();

	for( ; clause_itr != clause_end; ++clause_itr ) {
		if ( (*clause_itr)->name_ == "=" ) {
			combine((*clause_itr)->left_, (*clause_itr)->right_); 
		}
	}
}


list<node_c*> EquivalenceClass::list_parents(node_e* eq) {
	return eq->list_of_parents_;
}

list<node_c*> EquivalenceClass::list_parents(node_c* vertex) {
	int eq_class = curry_tree_.lookup(vertex->name_);

	return list_parents(equiv_classes_[eq_class]);
}

list<node_c*> EquivalenceClass::list_parents(string name) {
	int eq_class = curry_tree_.lookup(name);

	return list_parents(equiv_classes_[eq_class]);	
}

list<node_c*> EquivalenceClass::list_parents(int eq_class) {
	return list_parents(equiv_classes_[eq_class]);	
}


//Congruence Closure

void EquivalenceClass::findInitialClosure() {
	
	vector<node_c*> pending;
	
	//find closure
	for(int i = 0; i < equiv_classes_.size(); ++i) {
		if((equiv_classes_[i]->name_ != "end")) {	
			list<node_c*> parents = list_parents(equiv_classes_[i]->name_);
		
			pending.insert(pending.begin(), parents.begin(), parents.end());

			congruenceClosure(pending);
	
			pending.clear();
		}
	}
}

//congruence closure algorithm as described by Downey, Sethi, and Tarjan in "Variation on the Common Subexpression Problem"

bool EquivalenceClass::congruenceClosure(vector<node_c*> pending) {
	vector< pair<node_c*, node_c*> > combined;
	SignatureTable sig_tab;

	bool changes = false;
	while( !pending.empty() ) {
		combined.clear();
		
		for( int i = 0; i < pending.size(); ++i ) {
			//generate new sig for existing node
			if( pending[i]->left_ != NULL && pending[i]->right_ != NULL) {
				int sign_one = curry_tree_.lookup(find(pending[i]->left_)->name_);
				int sign_two = curry_tree_.lookup(find(pending[i]->right_)->name_);	
				
				pending[i]->signature_ = make_pair(sign_one, sign_two);
			}
			auto sign_location = sig_tab.query(pending[i]);
			if( sign_location == sig_tab.table_end() ) {
				sig_tab.enter(pending[i]);
			} else {
				pair<node_c*, node_c*> combo = make_pair(pending[i], sign_location->second);
				//combined.push_back(pending[i]);
				//combined.push_back(sign_location->second);
				combined.push_back(combo);
			}
		}
		pending.clear();

		for( int i = 0; i < combined.size(); ++i ) {
				if ( find(combined[i].first) != find(combined[i].second) ) {
					auto list_p_i = list_parents(find(combined[i].first));
					auto list_p_j = list_parents(find(combined[i].second));
				
//				if(list_p_i.size() < list_p_j.size()) {
						for(auto itr = list_p_i.begin(); itr != list_p_i.end(); ++itr ) {
							sig_tab.remove(*itr);
							pending.push_back(*itr);
						}

//						combine(find(combined[i]), find(combined[j]));
//						changes = true;
//					} else {

						for(auto itr = list_p_j.begin(); itr != list_p_j.end(); ++itr ) {
							sig_tab.remove(*itr);
							pending.push_back(*itr);
						}
					
						combine(find(combined[i].first), find(combined[i].second));
						changes = true;
					
//				}
			}
		}
	}
	return changes;
}
