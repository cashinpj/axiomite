#include <parse_node.h>

node_p* ParseNode::makeCopy() {
	node_p* new_node = new node_p;

	new_node->lookup_info_ = lookup_info_;
	new_node->token_ = token_;

	if(NULL != left_) {
		new_node->left_ = new vector<node_p*>;
		for(int i = 0; i < left_->size(); ++i) {
			new_node->left_->push_back(left_->at(i)->makeCopy());
			new_node->left_->at(i)->parent_ = new_node;
		}
	}

	if(NULL != right_) {
		new_node->right_ = new vector<node_p*>;
		for(int i = 0; i < right_->size(); ++i) {
			new_node->right_->push_back(right_->at(i)->makeCopy());
			new_node->right_->at(i)->parent_ = new_node;
		}
	}

	return new_node;
}


ParseNode::~ParseNode() {
    if(right_ != NULL) {
        delete right_;
    }

    if(left_ != NULL) {
        delete left_;
    }
}


void ParseNode::print() {

	cout << "Printing theorem" << endl;

    if (left_ != NULL) {
        for( int i = 0; i < left_->size(); ++i ) {
            left_->at(i)->print_rec(0);
        }
    }

	cout << "Printing rules" << endl;

    if (right_ != NULL) {
        for(int i = 0; i < right_->size(); ++i ) {
            right_->at(i)->print_rec(0);
        }
    }
}


void ParseNode::print_rec(int level) {
	for(int i = 0; i < level; ++i) {
		cout << "|\t";
	}

	++level;

    if ((token_ == "") && (lookup_info_ != NULL)) {
        lookup_info_->print();
    } else {
        cout << token_ << endl;
    }
	

	if(left_ != NULL) {
		for( int i = 0; i < left_->size(); ++i ) {
			left_->at(i)->print_rec(level);
		}
	}


	if(right_ != NULL) {
		for( int i = 0; i < right_->size(); ++i ) {
			right_->at(i)->print_rec(level);
		}
	}
}
