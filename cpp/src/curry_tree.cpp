#include <curry_tree.h>

//depthfirst search to find a curry node with a given name
node_c* CurryTree::findNode(string s) {
	stack<node_c*> next;

	for(int i = 0; i < root_.size(); ++i) {
		next.push(root_[i]);
	}

	while(!next.empty()) {
		node_c* current = next.top();
		next.pop();

		if(current->name_ == s) {
			return current;
		}

		if(current->left_ != NULL) {
			next.push(current->left_);
		}

		if(current->right_ != NULL) {
			next.push(current->right_);
		}

	}

	return NULL;
}

CurryTree::CurryTree(ParseTree& p, unordered_map<string, node_l*>* lookup) {
	init(p,lookup);
}

list<node_c*> CurryTree::list_parents(string name) {
	list<node_c*> parents;
	stack<node_c*> next;

	for(int i = 0; i < root_.size(); ++i) {
		next.push(root_[i]);
	}

	while(!next.empty()) {
		node_c* current = next.top();
		next.pop();

		if(NULL != current->left_) {
			if((current->left_->name_ == name) && ((current->name_ != "=") && (current->name_ != "->"))) {
				parents.push_back(current);
			}
			
			next.push(current->left_);
		}
		
		if(NULL != current->right_) {
			if((current->right_->name_ == name) && ((current->name_ != "=") && (current->name_ != "->"))) {
				parents.push_back(current);
			}

			next.push(current->right_);
		}
	}

	return parents;
}

/*
depricated
void CurryTree::listRecursive(string name, list<node_c*>& parents, node_c* root) {
	
	if( root->left_ != NULL ) {
		if ( root->name_ != "=" && root->left_->name_ == name ) {
			parents.push_back(root);
		}
		
		listRecursive(name, parents, root->left_);
	}
	
	if( root->right_ != NULL ) {
		if ( root->name_ != "=" && root->right_->name_ == name ) {
			parents.push_back(root);
		}
		
		listRecursive(name, parents, root->right_);
	}

}
*/

//curry every equivalence
void CurryTree::init(ParseTree& parse, unordered_map<string, node_l*>* lookup) {
	//Fill in curry table and add apply and end to the table
	auto end = lookup->end();
	int id_number;

	for (auto itr = lookup->begin(); itr != end; ++itr) {
		id_number = curry_table_.size();
		curry_table_[itr->first] = id_number;
	}

	id_number = curry_table_.size();
	curry_table_["end"] = id_number;

	auto itr_tree = parse.get_root()->left_->begin();
	auto end_tree = parse.get_root()->left_->end();
	
	for(; itr_tree != end_tree; ++itr_tree) {
		node_c* clause = new node_c;
			
		clause->name_ = (*itr_tree)->token_;
	
		if(clause->name_ == "=") {
			clause->type_ = EQUALS;
		} else if (clause->name_ == "->") {
			clause->type_ = IMPLIES;
		} else {
			cerr << "Unknown clause type" << endl;
		}

		//itr_tree is an iterator to a vector of pointers, which have pointers to vectors of pointers
		//many much deref
		clause->left_ = currification((*itr_tree)->left_->at(0));
		clause->right_ = currification((*itr_tree)->right_->at(0));

		clause->signature_ = make_pair(curry_table_[clause->left_->name_], curry_table_[clause->right_->name_]);
		
		root_.push_back(clause);	
	}
}	

void CurryTree::insert(node_p* root) { 
	
	node_c* clause = new node_c;
		
	clause->name_ = root->token_;

	if(clause->name_ == "=") {
		clause->type_ = EQUALS;
	} else if (clause->name_ == "->") {
		clause->type_ = IMPLIES;
	} else {
		cerr << "Unknown clause type" << endl;
	}

	//itr_tree is an iterator to a vector of pointers, which have pointers to vectors of pointers
	//many much deref
	clause->left_ = currification(root->left_->at(0));
	clause->right_ = currification(root->right_->at(0));

	clause->signature_ = make_pair(curry_table_[clause->left_->name_], curry_table_[clause->right_->name_]);
	
	root_.push_back(clause);	
}

//generate a curry tree from a single parse tree
node_c* CurryTree::currification(node_p* parse) {
	string apply_base = "apply";
	char buffer[100];
	int id_number;

	node_c* branch = new node_c;
	
	//if parse is a leaf node, return it	
	if (parse->left_ == NULL) {
/*
		//added
		node_c* current = new node_c;
		
		id_number = curry_table_.size();
		sprintf(buffer, "%d", id_number);
		current->name_ = apply_base + buffer;
		current->type_ = FUNC;

		curry_table_[current->name_] = id_number;	

		current->left_ = branch;
*/	
		//original
		branch->name_ = parse->lookup_info_->name_;
		branch->type_ = parse->lookup_info_->typing_;
/*		
		//added 
		node_c* end = new node_c;
		end->name_ = "end";
		end->type_ = CONST;

		current->right_ = end;
		
		int sig_one = curry_table_[current->left_->name_];
		int sig_two = curry_table_[current->right_->name_];

		current->signature_ = make_pair(sig_one, sig_two);
		
		branch = current;
*/
	} else {
		
		//create base for tree
		node_c* value = new node_c;
		node_c* current = branch;

		value->name_ = parse->lookup_info_->name_;
		value->type_ = parse->lookup_info_->typing_;

		branch->left_ = value;
	
		id_number = curry_table_.size();
		sprintf(buffer, "%d", id_number);
		branch->name_ = apply_base + buffer;
		branch->type_ = FUNC;

		curry_table_[branch->name_] = id_number;	
		
		//foreach child of parse generate a curry branch
		for( int i = 0; i < parse->left_->size(); ++i ) {
			//populate next branch with values for apply
			node_c* next_branch = new node_c;

			id_number = curry_table_.size();
			sprintf(buffer, "%d", id_number);
			next_branch->name_ = apply_base + buffer;	
			next_branch->type_ = FUNC;

			curry_table_[next_branch->name_] = id_number;

			next_branch->left_ = currification(parse->left_->at(i));

			//add new branch to tree and move current pointer forward
			current->right_ = next_branch;
			
			//set signature of branch
			int sig_one = curry_table_[current->left_->name_];
			int sig_two = curry_table_[current->right_->name_];
			
			current->signature_ = make_pair(sig_one, sig_two);

			current = next_branch;

		}

		//create end node and fix signature of current 
		node_c* end = new node_c;
		end->name_ = "end";
		end->type_ = CONST;

		current->right_ = end;
		
		int sig_one = curry_table_[current->left_->name_];
		int sig_two = curry_table_[current->right_->name_];
			
		current->signature_ = make_pair(sig_one, sig_two);
	}

	return branch;
}

void CurryTree::print() {
	cout << "Printing curry tree" << endl;

	for(int i = 0; i < root_.size(); ++i) {
		cout << "Tree #" << i << endl;
		printRecursive(root_[i], 0);
	}
/*	
	cout << endl << "Printing curry table" << endl;
	
	for(auto itr = curry_table_.begin(); itr != curry_table_.end(); ++itr) {
		cout << itr->first << ":" << itr->second << endl;
	}
*/
}

void CurryTree::printRecursive(node_c* root, int tabs) {

	for( int i = 0; i < tabs; ++i ) {
		cout << "|\t";
	}

	++tabs;
	
	cout << root->name_ << ":" << root->signature_.first << ":" << root->signature_.second << endl;

	if (root->left_ != NULL) {
		printRecursive(root->left_, tabs);
	}
	
	
	if(root->right_ != NULL) {
		printRecursive(root->right_, tabs);
	}
}
