#include <rete.h>

ReteGraph::ReteGraph() {
	graph_start_ = new node_r;

	graph_start_->type_ = GRAPH_START;
	graph_start_->value_ = "graph_start";

};

ReteGraph::ReteGraph(ParseTree& p) {
	graph_start_ = new node_r;

	graph_start_->type_ = GRAPH_START;
	graph_start_->value_ = "graph_start";

	node_p* root = p.get_root();

	for(int i = 0; i < root->right_->size(); ++i) {
		
		addPattern(root->right_->at(i));

	}

}

void ReteGraph::addPattern(node_p* root) {	

	if ((root != NULL) && (root->token_ == "=>")) {
	
		vector<node_r*> variable_joins;

		//create end node that holds a reference to the branch that created it
		node_r* end = new node_r;
		end->type_ = JOIN_END;
		end->value_ = "end";
		end->clause_ = root;

		//create list join to handle muliple equations for a single rule
		node_r* join_list = new node_r;
		join_list->type_ = JOIN_LIST;
		join_list->value_ = "join_list";
		
		join_list->children_.push_back(end);
		end->parents_.push_back(join_list);

		//generate the comparison nodes for each equation in the list
		for(int i = 0; i < root->left_->size(); ++i) {
			node_r* tail_branch = NULL;
			node_r* operation_node = NULL;
			
			bool chain_exists = false;
			node_p* current_parse = root->left_->at(i);

			//generate the lhs of equation
			generateBranch(graph_start_, current_parse->left_->at(0), tail_branch, variable_joins);

			//find if op exists already
			for(int j = 0; j < tail_branch->children_.size(); ++j) {
				if(tail_branch->children_.at(j)->value_ == current_parse->token_) {
					operation_node = tail_branch->children_.at(j);
					break;
				}
			}

			//if it does not exist create and add to branch
			if(NULL == operation_node) {
				operation_node = new node_r;
			
				if("=" == current_parse->token_) {
					operation_node->type_ = EQUALS;
					operation_node->value_ = "=";
				} else if ("->" == current_parse->token_) {
					operation_node->type_ = IMPLIES;
					operation_node->value_ = "->";
				} else{
					cout << "An error has occured generation graph" << endl;
					exit(1);
				}
			
				//add operation node to branch
				tail_branch->children_.push_back(operation_node);
				operation_node->parents_.push_back(tail_branch);
			}

			//starting at operation node geneate rhs into graph branch
			generateBranch(operation_node, current_parse->right_->at(0), tail_branch, variable_joins);
		
			//add rhs branch to end nodes if it is not already 
			for(int j = 0; j < join_list->parents_.size(); ++j) {
				if(join_list->parents_[j] == tail_branch) {
					chain_exists = true;
					break;
				}
			}

			if(!chain_exists) {
				join_list->parents_.push_back(tail_branch);
				tail_branch->children_.push_back(join_list);
			}

			//reset test variables for next loop
			tail_branch = NULL;
			operation_node = NULL;
			chain_exists = false;
		}

		//add all variable nodes to end join
		for(int i = 0; i < variable_joins.size(); ++i) {
			end->parents_.push_back(variable_joins[i]);
			variable_joins[i]->children_.push_back(end);
		}
	}
}

//generate branchs that do not already exist in the graph
void ReteGraph::generateBranch(node_r* graph_root, node_p* parse_root, node_r*& tail, vector<node_r*>& variable_joins) {
	int node_exists = -1;
	stack<node_p*> next; 

	node_r* current_graph = graph_root;
	next.push(parse_root);

	while(!next.empty()) {
		node_p* current_parse = next.top();
		next.pop();
		
		//check if node already exists
		//if it does skip creation of this node 
		for(int j = 0; j < current_graph->children_.size(); ++j) {
			if((current_graph->children_[j]->value_ == current_parse->lookup_info_->name_) && (JOIN_VAR != current_graph->children_[j]->type_)){
				node_exists = j;
				break;
			}
		}
		

		if(-1 != node_exists) {
			//move current graph pointer forward
			current_graph = current_graph->children_[node_exists];
			if(VAR == current_graph->type_) {
				//check for equivalent JOIN_VAR in variable_joins list
				//if it exists do nothing, otherwise add the existing one to 
				//list
				int exists = false;
				for(int i = 0; i < variable_joins.size(); ++i) {
					if(variable_joins.at(i)->value_ == current_graph->value_) {
						exists = true;
						break;
					}
				}

				if(!exists) {
					for(int i = 0; i < current_graph->children_.size(); ++i) {
						if(current_graph->value_ == current_graph->children_.at(i)->value_) {
							variable_joins.push_back(current_graph->children_.at(i));
						}
					}
				}
			}

			node_exists = -1;
		} else {
			//generate and populate new graph node
			node_r* temp = new node_r;

			temp->type_ = current_parse->lookup_info_->typing_;
			temp->value_ = current_parse->lookup_info_->name_;

			//if temp is a variable check if variable node is part of the 
			if(VAR == temp->type_) {

				node_r* var_join = NULL;
					
				for(int i = 0; i < variable_joins.size(); ++i) {
					if(temp->value_ == variable_joins[i]->value_) {
						var_join = variable_joins[i];
						break;
					}
				}

				if(NULL == var_join) {
					var_join = new node_r;
					var_join->value_ = temp->value_;
					var_join->type_ = JOIN_VAR;
					variable_joins.push_back(var_join);
				}
				
				temp->children_.push_back(var_join);
				var_join->parents_.push_back(temp);
			}

			node_exists = -1;
			
			//add node to graph
			current_graph->children_.push_back(temp);
			temp->parents_.push_back(current_graph);

			current_graph = temp;
		}
		//add all of the children of the current parse node to the stack 
		//children are added to the stack in reversing order to preserve the 
		//order of occurance
		if(NULL != current_parse->left_) {
			for(int i = current_parse->left_->size() - 1; i >=0; --i) {
				next.push(current_parse->left_->at(i));
			}
		}
	}
	tail = current_graph;
}


void ReteGraph::filterPattern(node_c* root, int equation_id, EquivalenceClass& e) {
	stack<node_r*> next_graph_node;
	stack<node_c*> next_curry_node;
	vector<node_c*> equal_to_current;

	//add graph start to stack
	next_graph_node.push(graph_start_);

	//add root and lhs to stack
	next_curry_node.push(root->left_);

	//use Depth first search to evaluate all of the curry nodes
	//evaluation takes longer than RETE due to not being able to 
	//assume a function, variable, constant or predicate is 
	//not equal to the test node based souly on name
	while (!next_graph_node.empty()) {
		//start with graph_start node and lhs of 
		//curry tree
		node_r* current_graph = next_graph_node.top();
		next_graph_node.pop();

		node_c* current_curry;
	
		do {
			if(!next_curry_node.empty()) {
				current_curry = next_curry_node.top();
				next_curry_node.pop();
			}else{
				break;
			}
		}while((current_curry->name_ == "end") && !next_curry_node.empty());
	//	cerr << "Testing " << current_curry->name_ << " and " << current_graph->value_ << ":" << current_graph->type_ << endl;

		switch(current_graph->type_) {
			case VAR:
				{	
					if(NULL != current_curry->left_) {
						if(string::npos != current_curry->left_->name_.find("apply")) {
							next_curry_node.push(current_curry->right_);
							next_curry_node.push(current_curry->left_);
							next_graph_node.push(current_graph);
						} else {
							if((PRED == current_curry->left_->type_) || (FUNC == current_curry->left_->type_)) {
								current_graph->matches_[e.find(current_curry->name_)->name_][0][equation_id] = current_curry;
							} else if(CONST == current_curry->left_->type_) {
								current_graph->matches_[e.find(current_curry->left_->name_)->name_][0][equation_id] = current_curry->left_;
								
								if(NULL != current_curry->right_) {
									for(int i = 0; i < current_graph->children_.size(); ++i) {
										next_curry_node.push(current_curry->right_);
									}
								}
							} else {
								cout << "Unknown typing" << endl;
								exit(1);
							}
							
							for(int i = 0; i < current_graph->children_.size(); ++i) {
								next_graph_node.push(current_graph->children_[i]);
							}
						}
					} else {
						current_graph->matches_[e.find(current_curry->name_)->name_][0][equation_id] = current_curry;
							
						if(NULL != current_curry->right_) {
							for(int i = 0; i < current_graph->children_.size(); ++i) {
								next_curry_node.push(current_curry->right_);
							}
						}
						
						for(int i = 0; i < current_graph->children_.size(); ++i) {
							next_graph_node.push(current_graph->children_[i]);
						}
					}
					break;
				}
			case CONST:
				{
					if(NULL != current_curry->left_) {
						switch(current_curry->left_->type_) {
							case FUNC:
							case PRED:
								{
									if(string::npos == current_curry->left_->name_.find("apply")) {
										if(e.isEqual(current_curry->name_, current_graph->value_)) {
											current_graph->matches_[e.find(current_curry->name_)->name_][0][equation_id] = current_curry;
							
											for(int i = 0; i < current_graph->children_.size(); ++i) {
												next_graph_node.push(current_graph->children_.at(i));
											}
											
											if((EQUALS != next_graph_node.top()->type_) && (IMPLIES != next_graph_node.top()->type_)) {
												for(int i = 0; i < current_graph->children_.size(); ++i) {
													next_curry_node.push(current_curry->right_);
												}
											}
										} 
									} else {
										if(e.isEqual(current_curry->left_->name_, current_graph->value_)) {
											current_graph->matches_[e.find(current_curry->left_->name_)->name_][0][equation_id] = current_curry;
							
											for(int i = 0; i < current_graph->children_.size(); ++i) {
												next_graph_node.push(current_graph->children_.at(i));
											}
											

											for(int i = 0; i < current_graph->children_.size(); ++i) {
												next_curry_node.push(current_curry->right_);
											}
										
										}
									}
									break;
								}
							case CONST:
								{
									if(e.isEqual(current_curry->left_->name_, current_graph->value_)) {
										current_graph->matches_[e.find(current_curry->left_->name_)->name_][0][equation_id] = current_curry;
							
										for(int i = 0; i < current_graph->children_.size(); ++i) {
											next_graph_node.push(current_graph->children_.at(i));
										}
										
										for(int i = 0; i < current_graph->children_.size(); ++i) {
											next_curry_node.push(current_curry->right_);
										}
									}
									break;
								}
							default:
								{
									cout << "Unexpected curry node type" << endl;
									exit(1);
								}
							}
					} else {
						if(e.isEqual(current_curry->name_, current_graph->value_)) {
							current_graph->matches_[e.find(current_curry->name_)->name_][0][equation_id] = current_curry;
							
							for(int i = 0; i < current_graph->children_.size(); ++i) {
								next_graph_node.push(current_graph->children_.at(i));
							}
						}
					}
					break;
				}
			case FUNC:
			case PRED:
				{
					bool matched = false;

					if(NULL != current_curry->left_) {
						if(string::npos != current_curry->left_->name_.find("apply") ) {
							//if the current curry tree is a nested function push the right
							//branch on to the stack then step down in to the nested function
							//push current graph onto stack to recheck it against nested function
							next_graph_node.push(current_graph);
							next_curry_node.push(current_curry->right_);
							next_curry_node.push(current_curry->left_);
							matched = true;
						} else { 
							equal_to_current = e.findAllEqual(current_curry->name_);

							for(int i = equal_to_current.size()-1; i >= 0; --i) {
								
								if(NULL != equal_to_current.at(i)->left_) {
									if(e.isEqual(equal_to_current.at(i)->left_->name_, current_graph->value_)) {

										current_graph->matches_[e.find(equal_to_current.at(i)->left_)->name_][0][equation_id] = current_curry;
						
										for(int x = 0; x < current_graph->children_.size(); ++x) {
											next_graph_node.push(current_graph->children_.at(x));
										}
										
										for(int x = 0; x < current_graph->children_.size(); ++x) {
											next_curry_node.push(equal_to_current.at(i)->right_);
										}
										matched = true;
									}
								}
								if(matched) {
									break;
								}
							}
						}
					} else {
						equal_to_current = e.findAllEqual(current_curry->name_);

						for(int i = equal_to_current.size()-1; i >= 0; --i) {
							
							if(NULL != equal_to_current.at(i)->left_) {
								if(e.isEqual(equal_to_current.at(i)->left_->name_, current_graph->value_)) {

									current_graph->matches_[e.find(equal_to_current.at(i)->left_)->name_][0][equation_id] = current_curry;
					
									for(int x = 0; x < current_graph->children_.size(); ++x) {
										next_graph_node.push(current_graph->children_.at(x));
									}

									for(int x = 0; x < current_graph->children_.size(); ++x) {
										next_curry_node.push(equal_to_current.at(i)->right_);
									}
									matched = true;
								}
							}
							if(matched) {
								break;
							}
						}
					}
						
					equal_to_current.clear();

					break;
				}	
			case EQUALS:
			case IMPLIES:
				{
					next_curry_node.push(current_curry);
					if(root->type_ == current_graph->type_) {
						for(int i = 0; i < current_graph->children_.size(); ++i) {
							next_graph_node.push(current_graph->children_.at(i));
							next_curry_node.push(root->right_);
						}
					}
					break;
				}
			case JOIN_VAR:
				{
					//look at each parent for matches from this equation
					//if each parent contains the same match add it to this node
					//and add children to stack
					
					string purposed_match = "";
					int parent_index;
					next_curry_node.push(current_curry);
					//first look for current equation in parents matches
					for(parent_index = 0; parent_index < current_graph->parents_.size(); ++parent_index) {
						auto itr = current_graph->parents_.at(parent_index)->matches_.begin();
						auto itr_end = current_graph->parents_.at(parent_index)->matches_.end();
						
						for(; itr != itr_end; ++itr) {
							if(itr->second.at(0).end() != itr->second.at(0).find(equation_id)) {
								purposed_match = itr->first;
								current_graph->matches_[purposed_match][parent_index][equation_id] = current_graph->parents_[parent_index]->matches_[purposed_match][0][equation_id];
								break;
							}
						}
					}
					
					//if the number of matches for the purposed match is equal to or larger than the number of 
					//variable slots then it contains a possible solution set. So add children of node to 
					//stack
					if(current_graph->parents_.size() <= current_graph->matches_[purposed_match].size()) {
						for(int i = 0; i < current_graph->children_.size(); ++i) {
							next_graph_node.push(current_graph->children_.at(i));
						}
					}
					break;
				}
			case JOIN_LIST:
				{
					next_curry_node.push(current_curry);
					bool add_match = true;
					//check to see if each branch (parent) has been satisfied by a match
					//if it has been then add children to stack
					for(int i = 0; i < current_graph->parents_.size(); ++i) {
						if (current_graph->parents_.at(i)->matches_.size() == 0) {
							add_match = false;
							break;
						}
					}
					
					//if matches are found for each branch of the graph, add them to the matches for the join
					if(add_match) {
						for(int i = 0; i < current_graph->parents_.size(); ++i) {
							for(auto itr_match = current_graph->parents_.at(i)->matches_.begin(); itr_match != current_graph->parents_.at(i)->matches_.end(); ++itr_match) {
								for(auto itr_parent = itr_match->second.begin(); itr_parent != itr_match->second.end(); ++itr_parent) {
									for(auto itr_eq = itr_parent->second.begin(); itr_eq != itr_parent->second.end(); ++itr_eq) {
										current_graph->matches_[""][i][itr_eq->first] = itr_eq->second;
									}
								}
							}
						}

						//push children onto the stack
						for(int i = 0; i < current_graph->children_.size(); ++i) {
							next_graph_node.push(current_graph->children_.at(i));
						}
					}	
					break;
				}
			case JOIN_END:
				{
					next_curry_node.push(current_curry);
					//generate list of possible solutions
					//first index is the parent branch id
					//second value is the equation id
					
					vector< vector<int> >possible_solution;
					vector< pair<string, node_c*> > replacements;
					
					//used to generate solution permutations
					vector<int> prefix;
					vector< vector<int> > permutations;

					int join_list_index;

					//grab possible solution from the join_list
					for(join_list_index = 0; join_list_index < current_graph->parents_.size(); ++join_list_index) {
						if(JOIN_LIST == current_graph->parents_.at(join_list_index)->type_) {
							break;
						}
					}

					//use list join as the basis for the list generation
					//removes possible partial matches
					
					//check to see if anything has been added.

					if(!current_graph->parents_.at(join_list_index)->matches_.empty()) {
						auto itr_parent = current_graph->parents_.at(join_list_index)->matches_.at("").begin();
						auto itr_parent_end = current_graph->parents_.at(join_list_index)->matches_.at("").end();

						possible_solution.resize(current_graph->parents_.at(join_list_index)->matches_.at("").size());
						
						for(; itr_parent != itr_parent_end; ++itr_parent) {
							auto itr_eq = itr_parent->second.begin();
							auto itr_eq_end = itr_parent->second.end();
							
							for(; itr_eq != itr_eq_end; ++itr_eq) {
								possible_solution[itr_parent->first].push_back(itr_eq->first);
							}
						}

						generatePermutations(possible_solution, permutations, prefix);
					
						for(int i = 0; i < permutations.size(); ++i) {
							if(!solutionExists(permutations[i], current_graph)) {
								if(generateValidReplacements(permutations[i], current_graph->parents_, replacements,e)){ 
									current_graph->accepted_solution_.push_back(permutations[i]);
									if(!replacementExists(replacements, current_graph, e)) {
										current_graph->accepted_replacement_.push_back(replacements);
										for(int x = 0; x < current_graph->clause_->right_->size(); ++x) {
											node_p* temp = current_graph->clause_->right_->at(x)->makeCopy();
											e.insertEquation(findAndReplace(temp, replacements));
										}
									}
									replacements.clear();
								}
							}
						}
					}				
					break;
				}
			case GRAPH_START:
				{
					//add all branches to stack 
					for(int i = 0; i < current_graph->children_.size(); ++i) {
						next_graph_node.push(current_graph->children_.at(i));
					}
						
					for(int i = 0; i < current_graph->children_.size(); ++i) {
						next_curry_node.push(current_curry);
					}
					break;
				}
			default:
				{
					cout << "Unknown typing " << current_graph->type_ << ":" << current_graph->value_ << endl;
					exit(1);
				}
		}
	}
	while(!next_curry_node.empty()) {
		next_curry_node.pop();
	}

	while(!next_graph_node.empty()) {
		next_graph_node.pop();
	}
}

bool ReteGraph::generateValidReplacements(vector<int>& purposed_solution, vector<node_r*>& parents, vector< pair<string, node_c*> >& reps, EquivalenceClass& e) {
	vector< pair<string, node_c*> > poss_rep;
	vector< pair<string, int> > variable_slots;
	map< string, pair<int, node_c*> > rep_count;
	int valid_matches = 0;

	cout <<	"Purposed: ";
	for(int i = 0; i < purposed_solution.size(); ++i) {
		cout << purposed_solution[i] << " ";
	}
	cout << endl;
	
	//for each eq in solution
	//if each variable node contains the same 
	//match its a valid replacement
	for(int i = 0; i < purposed_solution.size(); ++i) {
		for(int j = 0; j < parents.size(); ++j) {
			if(JOIN_VAR == parents.at(j)->type_) {
				variable_slots.push_back( make_pair( parents.at(j)->value_, parents.at(j)->parents_.size()));

				auto itr_match = parents.at(j)->matches_.begin();
				auto itr_match_end = parents.at(j)->matches_.end();

				for(; itr_match != itr_match_end; ++itr_match) {
					auto itr_parent = itr_match->second.begin();
					auto itr_parent_end = itr_match->second.end();
					
					for(; itr_parent != itr_parent_end; ++itr_parent) {
						if(itr_parent->second.find(purposed_solution.at(i)) != itr_parent->second.end()) {
							poss_rep.push_back( make_pair(parents.at(j)->value_, itr_parent->second.at(purposed_solution.at(i))));
						}
					}
				}
			}
		}
	}
	
	for(int i = 0; i < poss_rep.size(); ++i) {
		rep_count[poss_rep.at(i).first] = make_pair(0, poss_rep.at(i).second);
	}

	for(int i = 0; i < poss_rep.size(); ++i) {
		if(e.isEqual(rep_count[poss_rep.at(i).first].second, poss_rep.at(i).second)) {
			++rep_count[poss_rep.at(i).first].first;
		}
	}
	
	for(int i = 0; i < variable_slots.size(); ++i) {
		if(variable_slots.at(i).second == rep_count[variable_slots.at(i).first].first) {
			reps.push_back( make_pair(variable_slots.at(i).first, rep_count[variable_slots.at(i).first].second) );
			++valid_matches;
		}
	}

	if(variable_slots.size() == valid_matches) {
		return true;
	} else {
		return false;
	}
}

bool ReteGraph::sameReplacement(pair<string, node_c*> rep, pair<string, node_c*> rep2, EquivalenceClass& e) {
	bool same_string = rep.first == rep2.first;
	bool same_node = e.isEqual(rep.second, rep2.second);

	return same_string && same_node;
}

bool ReteGraph::replacementExists(vector< pair<string, node_c*> >& replacement, node_r* root, EquivalenceClass& e) {
	if(root->accepted_replacement_.empty()) {
		return false;
	} else {
		int matches;

		//check every solution for the current solution
		for(int i = 0; i < root->accepted_replacement_.size(); ++i) {
			matches = 0;
			for(int j = 0; j < replacement.size(); ++j) {
				if(sameReplacement(replacement.at(j), root->accepted_replacement_.at(i).at(j), e)) {
					++matches;
				}
			}

			if(replacement.size() == matches) {
				return true;	
			}
		}

		return false;
	}
}

bool ReteGraph::solutionExists(vector<int>& solution, node_r* root) {
	if(root->accepted_solution_.empty()) {
		return false;
	} else {
		bool exists = false;

		//check every solution for the current solution
		for(int i = 0; i < root->accepted_solution_.size(); ++i) {
			int found = true;
			for(int j = 0; j < solution.size(); ++j) {
				if(root->accepted_solution_.at(i).end() == std::find(root->accepted_solution_.at(i).begin(), root->accepted_solution_.at(i).end(), solution.at(j))) {
					found = false;
					break;
				}
			}
		
			if(found) {
				exists = found;
				break;
			}
		}

		return exists;
	}
}


void ReteGraph::generatePermutations(vector< vector<int> >& in, vector< vector<int> >& permute, vector<int> prefix) {
	if( prefix.size() == in.size() ){ 
		permute.push_back(prefix);
		return;
	} else {
		vector<int> choices(in[prefix.size()]);

		for(int i = 0; i < choices.size(); ++i) {
			vector<int> new_prefix(prefix);
			new_prefix.push_back(choices[i]);
			
			generatePermutations(in, permute, new_prefix);
		}
	}
}


node_p* ReteGraph::findAndReplace(node_p*& root, vector< pair<string, node_c*> >& reps) {
	stack<node_p*> next;

	if ( 0 < reps.size() ) {
		next.push(root);

		while(!next.empty()) {
			node_p* current = next.top();
			next.pop();

			for(int i = 0; i < reps.size(); ++i) {
				if(NULL != current->lookup_info_) {
					if ( reps.at(i).first == current->lookup_info_->name_ ) {
						variableReplace(current, reps.at(i).second);
					}
				}
			}

			if(NULL != current->left_) {
				for(int i = 0; i < current->left_->size(); ++i) {
					next.push(current->left_->at(i));
				}
			}

			if(NULL != current->right_) {
				for(int i = 0; i < current->right_->size(); ++i) {
					next.push(current->right_->at(i));
				}
			}
		}
	}

	return root;

}

void ReteGraph::variableReplace(node_p*& original, node_c* replacement) {
	//replace old node with new infomation and update children.

	if(NULL != original->left_) {
		delete original->left_;
	}

	if(NULL != original->right_) {
		delete original->right_;
	}

	//copy parent to prevent it from being overwritten
	
/*	
	node_p* parent = original->parent_;
	original = decurry(replacement);				
	original->parent_ = parent;
*/
	node_p* rep = decurry(replacement);
	original->token_ = rep->token_;
	original->lookup_info_ = rep->lookup_info_;
	original->left_ = rep->left_;
	original->right_ = rep->right_;

}

//turn curry tree into parse tree
node_p* ReteGraph::decurry(node_c* root) {

	node_p* new_node = new node_p;

	if(CONST == root->type_) {
		
		new_node->token_ = root->name_;
		new_node->lookup_info_ = lookup->at(new_node->token_);

	} else {
		new_node->token_ = root->left_->name_;
		new_node->lookup_info_ = lookup->at(new_node->token_);
		new_node->left_ = new vector<node_p*>;

		stack<node_c*> next;
	
		next.push(root->right_);	

		while(!next.empty()) {
			node_c* current_curry = next.top();
			next.pop();
	
			if(NULL != current_curry->left_) {
				if(string::npos == current_curry->left_->name_.find("apply")) {
					node_p* temp = new node_p;
					temp->token_ = current_curry->left_->name_;
					temp->lookup_info_ = lookup->at(temp->token_);
				
					new_node->left_->push_back(temp);
	
				} else {
					new_node->left_->push_back(decurry(current_curry->left_));
				}
	
				if(current_curry->right_->name_ != "end") {
					next.push(current_curry->right_);
				}
			}
		}	
	
		if(new_node->left_->size() > 0) {
			for(int i = 0; i < new_node->left_->size(); ++i) {
				new_node->left_->at(i)->parent_ = new_node;
			}
		} else {
			delete new_node->left_;
		}	
	}
	return new_node;
}

void ReteGraph::print() {
	stack<node_r*> evaluation;

	evaluation.push(graph_start_);

	while(!evaluation.empty()) {
		node_r* current = evaluation.top();
		evaluation.pop();

		cout << current->value_;

		switch(current->type_) {
			case CONST:
				cout << ":CONST";
				break;
			case FUNC:
				cout << ":FUNC";
				break;
			case PRED:
				cout << ":PRED";
				break;
			case JOIN_VAR:
				cout << ":JOIN_VAR";
				break;
			case JOIN_LIST:
				cout << ":JOIN_LIST";
				break;
			case EQUALS:
				cout << ":EQUALS";
				break;
			case JOIN_END:
				cout << ":JOIN_END";
				break;
			case VAR:
				cout << ":VAR";
				break;
			case IMPLIES:
				cout << ":IMPLIES";
				break;
			case GRAPH_START:
				cout << ":START";
			 	break;
			default:
				cout << ":UNKNOWN";
		}
	
		cout << " children: ";

		for(int i = 0; i < current->children_.size(); ++i) {
			cout << current->children_.at(i)->value_ << ":" << current->children_.at(i) << ", ";
			evaluation.push(current->children_.at(i));
		}
		cout << endl;
	}
}






































