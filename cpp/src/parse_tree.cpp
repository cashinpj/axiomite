#include <parse_tree.h>

/*
	Replace all functions with only constants as arguements with a constant 
	and add that substitution to the working memory.
	example:
		f(a) = b => ....
		
		replace f(a) with k and add f(a) = k to working knowledge

		rule becomes:
			k = b => ...
*/
void ParseTree::flattenRules() {
	vector<node_p*> new_knowledge;

	if(NULL != root_->right_) {
		for(int i = 0; i < root_->right_->size(); ++i) {
			stack<node_p*> next;
			next.push(root_->right_->at(i));

			while(!next.empty()) {
				node_p* current = next.top();
				next.pop();

				if(NULL != current->lookup_info_) {
					if((FUNC == current->lookup_info_->typing_) || (PRED == current->lookup_info_->typing_)) {
						flattenFunction(current, new_knowledge);
					}
				}

				if(NULL != current->left_) {
					for(int j = 0; j < current->left_->size(); ++j) {
						next.push(current->left_->at(j));
					}
				}
/*	
				if(NULL != current->right_) {
					for(int j = 0; j < current->right_->size(); ++j) {
						next.push(current->right_->at(j));
					}
				}
*/
			}
		}
	
		for(int i = 0; i < new_knowledge.size(); ++i) {
			root_->left_->push_back(new_knowledge.at(i));
		}
	}
}

void ParseTree::flattenFunction(node_p* root, vector<node_p*>& know) {
	//first flatten any other function or predicates 
	//then flatten current function
	if(NULL != root->left_) {
		for(int i = 0; i < root->left_->size(); ++i) {
			if ((FUNC == root->left_->at(i)->lookup_info_->typing_) || (PRED == root->left_->at(i)->lookup_info_->typing_)) {
				flattenFunction(root->left_->at(i), know);
			}
		}

		int constant_count = 0;
		//count constants if all constants flatten: else do nothing
		for(int i = 0; i < root->left_->size(); ++i) {
			if(CONST == root->left_->at(i)->lookup_info_->typing_) {
				++constant_count;
			}
		}

		if(root->left_->size() == constant_count) {
			//look for function in known
			int exists = -1;
			for(int i = 0; i < know.size(); ++i) {
				if(functionExistsInRule(root, know.at(i))) {
					exists = i;
					break;
				}
			}

			if(-1 == exists) {
				//replace everything and add to know
				node_p* new_function = new node_p;
				*new_function = *root;

				node_p* new_root = new node_p;
				
				//generate root for new rule
				new_root->token_ = "=";
				new_root->right_ = new vector<node_p*>;
				new_root->left_ = new vector<node_p*>;

				new_root->left_->push_back(new_function);
				new_function->parent_ = new_root;

				//generate new constant and add it to lookup table
				string constant_base = "flatten_constant";
				char buffer[100];
				int id_number = lookup->size();
				sprintf(buffer, "%d", id_number);

				node_l* constant = new node_l;

				constant->name_ = constant_base + buffer;
				constant->typing_ = CONST;
				constant->id_number_ = lookup->size();

				(*lookup)[constant->name_] = constant;
				
				node_p* new_const = new node_p;
				new_const->lookup_info_ = constant;
				new_const->parent_ = new_root;

				new_root->right_->push_back(new_const);

				//replace old function with new const
				root->lookup_info_ = new_const->lookup_info_;
				root->left_ = NULL;
				root->right_ = NULL;

				know.push_back(new_root);
			} else {
				//replace with constant previously generated
				root->lookup_info_ = know.at(exists)->right_->at(0)->lookup_info_;
				root->left_ = NULL;
				root->right_ = NULL;
			}
		}
	}
}

bool ParseTree::functionExistsInRule(node_p* function, node_p* root) {
	stack<node_p*> next;
	next.push(root);

	while(!next.empty()) {
		node_p* current = next.top();
		next.pop();
		int arg_count = 0;

		if(NULL != current->lookup_info_) {
			if(current->lookup_info_->name_ == function->lookup_info_->name_) {
				if(current->left_->size() == function->left_->size()) {
					for(int i = 0; i < current->left_->size(); ++i) {
						if(current->left_->at(i)->lookup_info_ == function->left_->at(i)->lookup_info_) {
							++arg_count;
						}
					}
				}

				if(arg_count == function->left_->size()) {
					return true;
				}
			}
		}

		if(NULL != current->left_) {
			for(int i = 0; i < current->left_->size(); ++i) {
				next.push(current->left_->at(i));
			}
		}
		
		if(NULL != current->right_) {
			for(int i = 0; i < current->right_->size(); ++i) {
				next.push(current->right_->at(i));
			}
		}
	}

	return false;

}

ParseTree::~ParseTree() {
	if( root_ != NULL ) { 
		delete root_;
	}
}

node_p* ParseTree::findById(int id) {
	return findById_helper(id, root_);
}

node_p* ParseTree::findById_helper(int id, node_p* current) {
	vector<node_p*> children;
	node_p* returnable = NULL;

	if ( current->lookup_info_ != NULL ) {
		if(current->lookup_info_->id_number_ == id) {
			return current;
		}
	} else {
		if ( current->left_ != NULL ) {
			children.insert(children.end(), current->left_->begin(), current->left_->end());
		}

		if ( current->right_ != NULL ) {
			children.insert(children.end(), current->right_->begin(), current->right_->end());
		}

		for(int i = 0; i < children.size(); ++i) {
			returnable = findById_helper(id, children[i]);
			if( returnable != NULL ) {
				return returnable;
			}
		}
	}

	return NULL;

}

void ParseTree::list(string name, vector<node_p*>* parent_list) {
	list_helper(name, parent_list, root_);
}

void ParseTree::list_helper(string name, vector<node_p*>* parent_list, node_p* current) {

	bool is_parent = false;

	//check if parent is a symbol and not an item	
	if( current->lookup_info_ != NULL ) {
		
		//check left side
		if ( current->left_ != NULL ) {
			for ( int i = 0; i < current->left_->size(); ++i ) {
				if ( (current->left_->at(i)->lookup_info_->name_ == name) ) {
					is_parent = true;
					break;
				}
			}
		}

		//check right side
		if( current->right_ != NULL ) {
			for ( int i = 0; i < current->right_->size(); ++i ) {
				if ( current->right_->at(i)->lookup_info_->name_ == name ) {
					is_parent = true;
					break;
				}
			}
		}

		//parent found add to list
		if(is_parent) {
			//parent is only added once, this should probably be a map rather than a vector
			bool is_in_set = false;
			for(int i = 0; i < parent_list->size(); ++i) {
				if(current->lookup_info_->name_ == parent_list->at(i)->lookup_info_->name_) {
					is_in_set = true;
					break;
				}
			}

			if(!is_in_set) {
				parent_list->push_back(current);
			}
		}
	}

	//recurse
	if ( current->left_ != NULL ) {
		for ( int i = 0; i < current->left_->size(); ++i ) {
			list_helper(name, parent_list, current->left_->at(i));
		}
	}

	if ( current->right_ != NULL ) {
		for ( int i = 0; i < current->right_->size(); ++i ) {
			list_helper(name, parent_list, current->right_->at(i));
		}
	}
}
