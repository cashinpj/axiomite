#include <iostream>
#include <vector>
#include <fstream>
#include <equivalence_class.h>
#include <rete.h>
using namespace std;
 
extern FILE *yyin;
extern FILE *yyout;
extern int yyparse(void);

extern unordered_map<string, node_l*>* lookup;
extern ParseTree parse;

int main(int argc, char** argv) {
    FILE* in_file;

    if ( argc < 2 ) {
        cout << "Please enter a file to parse." << endl;
        return -1;
    }

    in_file = fopen(argv[1], "r");
    if ( !in_file ) {
        cout << "Error opening file." << endl;
        return -1;
    }

    yyin = in_file;

    do {
        yyparse();
    }while(!feof(yyin));

	//flattening rules
	parse.flattenRules();

	cerr << "Making eq class" << endl;
	EquivalenceClass e(parse, lookup);

	cerr << "Making rete graph" << endl;
	ReteGraph g(parse);
//	g.print();						

	int working_size;
	do {
		cerr << "Finding closure" << endl;
		e.findInitialClosure();
		
		working_size = e.get_curry_tree().get_root().size();
		cerr << "Filtering knowledge" << endl;
		for(int i = 0; i < working_size; ++i) {
			g.filterPattern(e.get_curry_tree().get_root().at(i), i, e);
		}

	}while(working_size != e.get_curry_tree().get_root().size());

	e.print();

    return 0;
}
