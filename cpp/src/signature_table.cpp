#include <signature_table.h>

void SignatureTable::enter(pair<int, int> sign, node_c* vertex) {
	if ( table_.find(sign) == table_.end() ) {
		table_[sign] = vertex;
	}
}

void SignatureTable::enter(node_c* vertex) {
	if ( table_.find(vertex->signature_) == table_.end() ) {
		table_[vertex->signature_] = vertex;
	}
}

void SignatureTable::remove(pair<int, int> sign) {
	table_.erase(sign);
}

void SignatureTable::remove(node_c* vertex) {
	table_.erase(vertex->signature_);
}

map<pair<int,int>, node_c*>::iterator SignatureTable::query(pair<int,int> sign) {
	return table_.find(sign);
}

map<pair<int,int>, node_c*>::iterator SignatureTable::query(node_c* vertex) {
	return table_.find(vertex->signature_);
}

void SignatureTable::print() {
	for(auto itr = table_.begin(); itr != table_.end(); ++itr) {
		cout << itr->first.first << ":" << itr->first.second << ":" << itr->second->name_ << endl;
	}
}
