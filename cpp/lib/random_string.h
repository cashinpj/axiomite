#ifndef _RANDOM_STRING_H_
#define _RANDOM_STRING_H_

#include <cstdlib>
#include <ctime>
using namespace std;

class RandomString {
	public:
		RandomString(): char_set_("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") { srand(time(NULL)); }
		
		string randomString(int);
	private:
		string char_set_;
};

string RandomString::randomString(int length) {
	string result;
	result.resize(length);

	for( int i = 0; i < length; ++i ) {
		result[i] = char_set_[ rand() % char_set_.length() ];
	}

	return result;
}


#endif
