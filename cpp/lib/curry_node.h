#ifndef _CURRY_NODE_H_
#define _CURRY_NODE_H_

typedef struct CurryNode {
    CurryNode(): name_(""), left_(NULL), right_(NULL) { signature_ = make_pair(-1,-1); }
    ~CurryNode() { delete left_; delete right_; }

    string name_;
	int type_;

    pair<int,int> signature_;

    CurryNode* left_;
    CurryNode* right_;

}node_c;

#endif
