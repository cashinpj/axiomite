#ifndef _PARSE_TREE_H_
#define _PARSE_TREE_H_

#include <parse_node.h>
#include <vector>
#include <unordered_map>
#include <stack>
#include "../axio.tab.h"

extern unordered_map<string, node_l*>* lookup;

class ParseTree {
	public:
		ParseTree(): root_(NULL) {}
		~ParseTree();

		//parse tree functions
		void insert(node_p* node) { root_ = node; }
		void print() { root_->print(); }
		node_p* get_root() { return root_; }
		node_p* findById(int);	
		//take in a name of a declared item and fill a vector will all of its parents
		void list(string, vector<node_p*>*);

		//replace each function of only constants with a constant in the rules
		void flattenRules();

	private:
		void flattenFunction(node_p*, vector<node_p*>&);
		bool functionExistsInRule(node_p*, node_p*);
		//helper functions allow for recursive calls
		void list_helper(string, vector<node_p*>*, node_p*);
		node_p* findById_helper(int, node_p*);
		node_p* root_;
};

#endif
