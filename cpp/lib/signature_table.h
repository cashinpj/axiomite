#ifndef _SIGNATURE_TABLE_H_
#define _SIGNATIRE_TABLE_H_

#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <curry_tree.h>
using namespace std;


class SignatureTable {
	public:
		void enter(pair<int, int>, node_c*);
		void enter(node_c*);

		void remove(pair<int, int>);
		void remove(node_c*);

		map<pair<int,int>, node_c*>::iterator query(pair<int, int>);
		map<pair<int,int>, node_c*>::iterator query(node_c*);
		
		//accessors
		map<pair<int,int>, node_c*>::iterator table_begin() { return table_.begin(); }
		map<pair<int,int>, node_c*>::iterator table_end() { return table_.end(); }
		
		void print();
	private:
		map<pair<int,int>, node_c*> table_;
};

#endif
