#ifndef _PARSE_NODE_H_
#define _PARSE_NODE_H_

#include <iostream>
#include <vector>
#include <string>
#include <lookup_node.h>
using namespace std;

typedef struct ParseNode {
	ParseNode(): lookup_info_(NULL), right_(NULL), left_(NULL), token_(""), parent_(NULL) {}
	~ParseNode();
	void print();
	void print_rec(int);

	ParseNode* makeCopy();

	node_l* lookup_info_;
	string token_;

	ParseNode* parent_;
	vector<ParseNode*>* right_;
	vector<ParseNode*>* left_;
} node_p;

#endif
