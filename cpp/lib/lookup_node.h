#ifndef _VERTEX_NODE_H_
#define _VERTEX_NODE_H_

#include <string>
#include <iostream>
using namespace std;

typedef struct LookupNode {
	LookupNode(): name_(""), typing_(-1), id_number_(-1) {}

	void print() { cout << name_ << ":" << typing_ << ":" << id_number_ << endl; }

	string name_;
	int typing_;
	int id_number_;
} node_l;

#endif
