#ifndef _RETE_H_
#define _RETE_H_

#include <parse_tree.h>
#include <curry_tree.h>
#include <equivalence_class.h>
#include <list>
#include <queue>
#include <stack>
#include <cstdlib>
#include <iostream>
#include "../axio.tab.h"
#include <map>
#include <algorithm>

enum GRAPH_NODE_TYPE { JOIN_VAR, JOIN_LIST, JOIN_END, GRAPH_START };

extern unordered_map<string, node_l*>* lookup;

typedef struct ReteNode {
	ReteNode():type_(0), value_(""), clause_(NULL) {}

	int type_;
	string value_;

	//pointer to rule to be added to 
	node_p* clause_;

	//maps matched value -> parent id -> equation_id -> node_c*
	//for single parent nodes parent id defaults to 0;
	//for list join matched value defaults to empty string ""
	map<string, map<int, map<int, node_c*> > > matches_;

	//only used by the end nodes to track which equations have fired rules
	vector< vector<int> > accepted_solution_;
	vector< vector< pair<string, node_c*> > > accepted_replacement_;

	vector<ReteNode*> parents_;
	vector<ReteNode*> children_;

} node_r;

class ReteGraph {
	public:
		ReteGraph();
		ReteGraph(ParseTree&);

		void filterPattern(node_c*, int, EquivalenceClass&);
		
		void print();
	private:
		bool generateValidReplacements(vector<int>&, vector<node_r*>&, vector< pair<string, node_c*> >&, EquivalenceClass&);
		bool solutionExists(vector<int>&, node_r*);
		bool replacementExists(vector< pair<string, node_c*> >&, node_r*, EquivalenceClass&);
		void generatePermutations(vector< vector<int> >&, vector< vector<int> >&, vector<int>);
	
		bool sameReplacement(pair<string, node_c*>, pair<string, node_c*>, EquivalenceClass&);

		void addPattern(node_p*);
		
		node_p* findAndReplace(node_p*&, vector< pair<string, node_c*> >&);
		node_p* decurry(node_c*);
		
		void variableReplace(node_p*&, node_c*);	
		//creates a filter branch base, the last pointers mark the beginning
		//and the end of the branch.  The vector contains a list of variables
		//that have join nodes.
		void generateBranch(node_r*, node_p*, node_r*&, vector<node_r*>&);
		
		node_r* graph_start_;
};

#endif
