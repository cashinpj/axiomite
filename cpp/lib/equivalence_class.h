#ifndef _EQ_CLASS_H_
#define _EQ_CLASS_H_

#include <curry_tree.h> 
#include <parse_tree.h>
#include <lookup_node.h>
#include <unordered_map>
#include <list>
#include <signature_table.h>

typedef struct EquivalenceNode {
	string name_;
	int id_number_;
	int rank_;
	
	list<node_c*> list_of_parents_;

	node_c* location_;
	
	EquivalenceNode* representative_;
} node_e;


class EquivalenceClass {
	public:
		EquivalenceClass(ParseTree& p, unordered_map<string, node_l*>* l) { init(p, l); }
		void init(CurryTree&);
		void init(ParseTree&, unordered_map<string, node_l*>*);
		
		template <class S>
		bool isEqual(S s1, S s2) { return find(s1) == find(s2); }
		CurryTree get_curry_tree() { return curry_tree_; }
		
		vector<node_c*> findAllEqual(string);

		node_e* find(int);
		node_e* find(string);
		node_e* find(node_c*);
		node_e* find(node_e*);
		
		list<node_c*> list_parents(string);
		list<node_c*> list_parents(int);
		list<node_c*> list_parents(node_c*);
		list<node_c*> list_parents(node_e*);

		//combine == union
		void combine(int, int);
		void combine(string, string);
		void combine(node_c*, node_c*);
		void combine(node_e*, node_e*);

		void print();

		void insertEquation(node_p* root) { curry_tree_.insert(root); init(curry_tree_); }

		//FOL algorithms
		void unionFind();
		void findInitialClosure();
		bool congruenceClosure(vector<node_c*>);
	private:
		CurryTree curry_tree_;
		vector<node_e*> equiv_classes_;
};

#endif
