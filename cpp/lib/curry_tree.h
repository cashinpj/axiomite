#ifndef _CURRY_TREE_H_
#define _CURRY_TREE_H_

#include <map>
#include <unordered_map>
#include <parse_tree.h>
#include <utility>
#include <curry_node.h>
#include <list>
#include <stack>
#include "../axio.tab.h"

class CurryTree {
	public:
		CurryTree() {}
		CurryTree(ParseTree&, unordered_map<string, node_l*>*);
	
		void init(ParseTree&, unordered_map<string, node_l*>*);
		
		list<node_c*> list_parents(string);
		
		//accessors
		int lookup(string name) { return curry_table_[name]; }
		map<string, int>::iterator table_begin() { return curry_table_.begin(); }
		map<string, int>::iterator table_end() { return curry_table_.end(); }			
		int table_size() { return curry_table_.size(); }
		vector<node_c*> get_root() { return root_; }
		
		vector<node_c*>::iterator root_begin() { return root_.begin(); }
		vector<node_c*>::iterator root_end() { return root_.end(); }		
	
		void insert(node_p*);

		void print();
		node_c* findNode(string);

	private:
		node_c* currification(node_p*);
		void printRecursive(node_c*, int);
		void listRecursive(string, list<node_c*>&, node_c*);

		map<string, int> curry_table_;
		vector<node_c*> root_;
};

#endif
