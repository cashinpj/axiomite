\documentclass{article}
\usepackage{cite}

\begin{document}

\section{Introduction}

Satisfiability(SAT) continues to be an important topic in computing.  In it's 
simplest form, a SAT problem consists of a set of literals, containing 
variables and constants, to which we want to find an assignment for each 
variable such that the entire problem evaluates to true \cite{barrett18, arkin89}.
An evolution of SAT was the addition of Theory solvers, which allow the entire
system to reason about specific theories, such as integers, real numbers, lists,
or binary arrays, creating Satisfiability Modulo Theory(SMT) solvers.  Program
verification, model checking, optimization, and many more fields have deployed
SMT solvers to solve difficult problems \cite{barrett18, arkin89}. 
Modern SMT solvers contain
several individual T-solvers, as well as methods for mixed theory 
solutions\cite{boogie11, demoura08, barrett11, bobot08, barrett18}.

A theory solver(T-Solver) evaluates a model for T-Satisfiability(T-SAT).  A 
model is considered T-SAT if \(\Lambda\), the set of internally stored
literals, is satisfiable with respect to the theory \(T\) \cite{arkin89, barrett18}.  
Modern theory solvers are constructed around a generic, 
\textit{lazy} architecture based on a conflict driven clause learning(CDCL) 
engine, commonly referred to as DPLL(T)\cite{barrett18, dutertre06, ganzinger04, biere09}.  
These T-solvers communicate using a standardized interface which allows 
most SMT solvers to maintain a modular design\cite{barrett18,demoura08,barrett11,
bobot08,audemard09}.  

%TODO:What is different?
%TODO: Find citations for DPLL(X) and Clingo 5

Although, SMT solvers are modular and T-solvers can be constructed
using a generic framework, a generic T-solver does not exist \cite{barrett18, 
boogie11, chlipala13}.  
To this end, we present Axiomite, a general theory solver.  Unlike traditional 
solvers, Axiomite takes a theory as input at run time.  The theory is presented
as a set of rules which model the logical dependencies of the system.  These 
rules used as the basis for a forward-chaining expert system.  For 
Axiomite, we implemented a simplified version of the RETE production system
\cite{forgy82}.  In addition to a set of rules, Axiomite is handed a starting
set of theory literals, \(\Lambda_0\), from which it will attempt to reach a
conclusion.

Our implementation of RETE allows Axiomite to modify \(\Lambda\) by \textit{firing}
rules until no new literals can be added.  Each rule consists of an 
antecedent and a consequent.  The antecedent is a single literal or conjunction of
literals.  If each literal in the antecedent can be found within \(\Lambda\),
then the consequent is evaluated or \textit{fired}\cite{forgy82, nakatsu09, abraham05}.
Each consequent contains a set of one or more \textit{prototype} literals, 
generalized literals containing variables with no assigned value.
When a consequent fires, each variable in the consequent is assigned a value 
and the resulting literals are added to \(\Lambda\).  This process continues 
until no additional literals can be added to \(\Lambda\).  
Once a static set of literals has been found, Axiomite returns an \(\Lambda\) 
and an equivalence table(EQT) representing any equivalence relationship between
terms in \(\Lambda\).  Equivalence relationships are evaluated using a classic
congruence closure algorithm \cite{downey80}.

Our initial implementation of Axiomite focuses on 
uninterpreted functions with equality \cite{kroening08}.  This allows Axiomite 
to maintain a simple internal design, which can become more complex based on the 
theory given at run time.

In the remainder of this paper, We:

\begin{itemize}
	\item Present a detailed description of Axiomite.
	\item Evaluate Axiomite run time and correctness on a test suite.
	\item Discuss implementation specifics and limitations.
\end{itemize}

In Chapter 2, we describe the constituent algorithms of Axiomite and their 
expected run times.  Chapter 3 discusses the implementation details of Axiomite,
Chapter 4 explains how Axiomite relates to the previous work in SMT and 
production systems, and concludes with future work and limitations.

\section{Related Work}

\subsection{SAT and SMT Solvers}

Since useful problems from optimization, verification, and model checking
can be reduced to satisfiability(SAT) problems, there has been continued 
development on SAT solvers\cite{harrison07, monniaux16, barrett18}.  Even though there 
are many different solvers available
\cite{boogie11, demoura08, barrett11, bobot08, audemard09}, most solvers rely 
on an implementation of the Davis-Putnam-Logemann-
Loveland algorithm(DPLL) \cite{davis60, davis62}.  DPLL is an exhaustive search algorithm for Boolean
SAT problems, and forms the base of modern conflict-driven clause learning(CDCL)
solvers \cite{biere09,marques09,dutertre06, ganzinger04}.  The DPLL 
algorithm searches a decision tree of possible 
variable assignments, looking for a satisfying solution.  In 
general, the algorithm recursively makes decisions by assigning a variable 1 or 0, 
propagating the effects of the assignment, until it either finds a conflict 
(UN-SAT) or has assigned a value to all variables (SAT).
If an assignment is UNSAT, then DPLL backtracks to a previous decision, 
changes the variable assignment, and then continues.  If there is no 
decision to backtrack to, then the entire problem is UNSAT.  The propagation of variable 
assignments is driven by the repeated application of the \textit{unit clause rule}.  A unit clause
is any clause with 1 unassigned variable.  When a decision is made, each unit clause updated
so that it evaluates to 1.  This continues until either all variables have been assigned, 
a conflict occurs, or another decision has to be made.  Since DPLL exhaustively searches
for a satisfying assignment, it is complete for Boolean SAT problems \cite{davis60, davis62}.

The CDCL algorithm deploys a slightly more sophisticated backtracking and clause
propagation algorithm designed to speed up the search for satisfying assignments
\cite{dutertre06, biere09, marques09}.   The major advantage of CDCL over the 
classic DPLL algorithm is the ability to learn new clause whenever a conflict 
is discovered.  When applying the unit clause rule for propagation, the CDCL
solver tracks the implication graph for a decision.  When a conflict is reached,
the implication graph is traversed back to the most recent decision.  As the 
implication graph is traversed, a learnt clause is constructed.  The clause is
then appended to the original problem \cite{dutertre06, ganzinger04, biere09, marques09}.  These
learnt clauses effectively reduce the search space of the solver, speeding it up 
against the standard DPLL algorithm \cite{biere09, marques09}.  

After the construction of SAT solvers for Boolean problems, there was a push into 
first-order logic\cite{barrett18, kroening08}.  This led to the development of
satisfiability modulo theory(SMT) solvers \cite{barrett11, dutertre06, ganzinger04}.  
Modern SMT solvers are constructed from a SAT engine, typically based on a 
CDCL solver, with auxiliary theory solvers(T-solver) \cite{boogie11, demoura08, 
barrett11, bobot08, audemard09, barrett11}. Each T-solver is responsible for 
a single theory, such as reals, integers, or binary arrays.  The T-solvers 
maintain a internal set of theory literals against which a model is checked
for satisfiability (T-SAT).  A model is considered T-SAT, if each theory 
literal is true under the given model.  If a model is not T-SAT, then the T-solver returns
the conflict clause to the SAT engine \cite{dutertre06, ganzinger04, barrett18}.
The returned clause is used as an learnt clause for the SAT engine's CDCL 
algorithm \cite{biere09, marques09, barrett18}.

Each T-solver communicates with the SAT engine using a standard interface, allowing
for the creation of the solvers using generic frameworks.  The most common 
of these frameworks is known as DPLL(T) \cite{dutertre06, ganzinger04}.  
DPLL(T) solvers consist of two parts, a generic implementation of a DPLL
and a \(Solver_T\).  The \(Solver_T\) consists of specialized algorithms 
designed to reason about the given theory \cite{dutertre06, ganzinger04}.
The \(Solver_T\) maintains a set of literals, which are either passed
as input or are consequences of other literals and axioms of the theory.
The solver also must support backtracking, in the event a conflict 
is found, and the creation of the explanation clause \cite{dutertre06,
ganzinger04}.  Although, solvers have traditionally be constructed in this manner, 
there has been movement towards making more specialized T-solvers which 
trade the modularity of the DPLL(T) framework for efficiency \cite{barrett18}.

\subsection{Expert Systems}

Expert systems attempt to encode human expertise concerning a specific 
problem domain.  This knowledge is encoded as a set of rules which are 
followed to a conclusion based on provided input.  Each rule dictates 
an action to be taken according to the current 'working knowledge', 
data given as input or added by a rule.  The system progresses as
long as it can fire rules.  Once it has exhausted its options, it
either returns solution or requests more information from the user \cite{nakatsu09, abraham05}. 
Expert systems have been deployed to aid in diagnostics, optimization,
and signal processing \cite{lee19, ahmad16, ju20, shortliffe12, rassinoux96, 
barnett87}.

Expert systems fall into two main designs: forward-chaining or backward-chaining.  
A forward-chaining expert system starts with input and fires rules until it 
arrives at a conclusion.  By contrast, a backward-chaining expert system 
starts with a hypothesis and follows rules in order to construct a justification
for that hypothesis \cite{nakatsu09, abraham05}.  Regardless of the 
type of system, rules are typically written as antecedent-consequent pairs.
If all parts of the antecedent are found in working knowledge then the 
associated consequent is fired.  This can involve adding or deleting 
clauses from working knowledge, requesting additional information, or 
even running secondary routines \cite{nakatsu09, abraham05, shortliffe12, 
lee19, paschke09}.  Modern expert systems frequently operate in domains 
where input values contain noise.  Noisy systems rely on routines to 
clean inputs before passing them to the expert system.  Another 
simple approach to handling noisy data is to consider values in ranges 
rather than precise values \cite{nakatsu09, abraham05, ahmad16, ju20, 
lee19}.

Production systems conceptually extend the idea of expert systems to other
activities.  Rather than attempting to draw and conclusion, productions 
systems set out a set of rules, referred to as productions, which indicate
an action to take given the current working knowledge \cite{forgy82, wright03,
miranker87, batory94}.  Similar to rules from expert systems, each 
production contains a antecedent and a consequent, however in this case
a consequent can be an action.  Due to this additional capability, 
production systems have been used to create action planning systems
\cite{wright03, miranker87, miranker91}.  There are small differences between
production and expert systems in practice, forward-chaining expert
systems in particular can be constructed from a production system
with little modification \cite{nakatsu09, abraham05, batory94, forgy82}.

Although, RETE is considered the original production system \cite{forgy82}, 
there has been development on faster methods \cite{wright03, miranker87, batory94}.  
TREAT follows a similar approach to RETE when attempting to satisfy 
productions.  However, unlike RETE it tracks prior matches such that they
can be removed from working knowledge should a production require it.
TREAT also tracks partial matches by rough time stamp, allowing for 
quicker assessment of new information and deletion of unnecessary 
matches\cite{miranker87}.  LEAPS follows an even faster approach borrowed 
from databases.  Productions are constructed into specialized containers 
which allow for the lazy evaluation of working knowledge \cite{batory94}.

\bibliographystyle{plain}
\bibliography{biblio}
\end{document}
